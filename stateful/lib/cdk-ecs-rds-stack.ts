import {CdkEcsRdsStackProps} from "./cdk-ecs-rds-stack-props";
import {ResourceGroup} from "./resource-group";
import {Construct, Stack} from "@aws-cdk/core";
import {PostgresqlRds} from "./postgresql-rds/postgesql-rds";
import {EcsFargate} from "./ecs-fargate/ecs-fargate";


export class CdkEcsRdsStack extends Stack {
    constructor(scope: Construct, id: string, props: CdkEcsRdsStackProps) {
        super(scope, id, props);
        //TODO: validate "props" to fail fast.
        console.log('props:' + JSON.stringify(props));

        // Build Resource Group
        const resourceGroup = new ResourceGroup(this, props).create();

        // Build postgresqlRds instance
        const postgresqlRds = new PostgresqlRds(this, props);

        // Build ecsFargate cluster
        const ecsFargate = new EcsFargate(this, props);

    }
}
