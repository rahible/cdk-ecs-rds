import {Stack} from "@aws-cdk/core";
import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import {Peer, Port, SecurityGroup, SubnetType, Vpc} from "@aws-cdk/aws-ec2";
import {Cluster} from "@aws-cdk/aws-ecs";
import {ApplicationLoadBalancer, ApplicationProtocol, ListenerAction} from "@aws-cdk/aws-elasticloadbalancingv2";

export class EcsFargate {
    constructor(scope: Stack, props: CdkEcsRdsStackProps) {
        // find vpc we want to put the RDS in
        const vpc = Vpc.fromLookup(scope, 'existingVpcEcs', {
            isDefault: false,
            tags: props.vpcTargetTag
        });

        const cluster = new Cluster(scope, `${props.tags?.environment}EcsCluster`, {
            vpc,
            clusterName: `${props.tags?.environment}-ecs-cluster`
        });

        const securityGroup = new SecurityGroup(scope, `${props.tags?.environment}AlbSg`, {
            vpc,
            securityGroupName: `${props.tags?.environment}-alb-sg`,
            description: `Security group for the ${props.tags?.environment} ALB fronting the ${props.tags?.environment} ECS Fargate Cluster`
        });
        const restrictedIp = props.restrictedIp ?? '0.0.0.0/0';
        securityGroup.addIngressRule(Peer.ipv4(restrictedIp), Port.tcp(80));

        const applicationLoadBalancer = new ApplicationLoadBalancer(scope, `${props.tags?.environment}Alb`, {
            vpc,
            securityGroup,
            loadBalancerName: `${props.tags?.environment}-alb`,
            // An ALB can only be connected to one subnet in an AZ
            vpcSubnets: {
                onePerAz: true,
                subnetType: SubnetType.PUBLIC
            }
        });

        const listener = applicationLoadBalancer.addListener(`${props.tags?.environment}HttpListener`, {
            port: 80,
            protocol: ApplicationProtocol.HTTP,
            defaultAction: ListenerAction.fixedResponse(404)
        });
    }
}