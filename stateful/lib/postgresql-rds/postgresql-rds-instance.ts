import {StackMemberCreator} from "../stack-member-creator";
import {
    Credentials,
    DatabaseInstance,
    DatabaseInstanceEngine,
    DatabaseInstanceProps,
    PostgresEngineVersion
} from "@aws-cdk/aws-rds";
import {CfnOutput, Stack} from "@aws-cdk/core";
import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import {ISecret} from "@aws-cdk/aws-secretsmanager";
import {
    Connections,
    InstanceClass,
    InstanceSize,
    InstanceType,
    Port,
    SecurityGroup,
    SubnetType,
    Vpc
} from "@aws-cdk/aws-ec2";
import {Effect, Policy, PolicyStatement, Role, ServicePrincipal} from "@aws-cdk/aws-iam";

export class PostgresqlRdsInstance implements StackMemberCreator {
    constructor(private scope: Stack,
                private props: CdkEcsRdsStackProps,
                private databaseAdminSecret: ISecret,
                private securityGroups: SecurityGroup[]) {
    }

    create(): DatabaseInstance {
        // find vpc we want to put the RDS in
        const vpc = Vpc.fromLookup(this.scope, 'existingVpcRds', {
            isDefault: false,
            tags: this.props.vpcTargetTag
        });

        // database security group
        const databaseSecurityGroup = new SecurityGroup(this.scope, `${this.props.tags?.environment}DatabaseSg`, {
            vpc,
            allowAllOutbound: true,
            description: 'security group postgresql',
            securityGroupName: `${this.props.tags?.environment}-database-sg`
        });

        // allow traffic from the bastion security group
        databaseSecurityGroup.connections.allowFrom(
            new Connections({
                securityGroups: this.securityGroups
            }),
            Port.tcp(5432),
            'Allow traffic to PostgreSQL default port'
        );

        // configure RDS instance
        const rdsConfig: DatabaseInstanceProps = {
            engine: DatabaseInstanceEngine.postgres({version: PostgresEngineVersion.VER_12_3}),
            // optional, defaults to m5.large
            instanceType: InstanceType.of(InstanceClass.BURSTABLE2, InstanceSize.SMALL),
            instanceIdentifier: `${this.props.tags?.environment}-instance`,
            databaseName: `${this.props.tags?.environment}`,
            vpc: vpc,
            vpcSubnets: {
                onePerAz: true,
                subnetType: SubnetType.PRIVATE
            },
            maxAllocatedStorage: 200,
            securityGroups: [databaseSecurityGroup],
            credentials: Credentials.fromSecret(this.databaseAdminSecret), // Get both username and password from existing secret
            iamAuthentication: true, // password-less authentication for the application

        }

        // create the instance
        const rdsInstance = new DatabaseInstance(this.scope, `${this.props.tags?.environment}Instance`, rdsConfig);

        // create policy to connect through a role without the password.
        // TODO: Remove hard coded 'record_collection_api' user name - retrieve username from secret passed in or just pass in the username?
        const dbUserArn = `arn:aws:rds-db:${Stack.of(this.scope).region}:${Stack.of(this.scope).account}:dbuser:${rdsInstance.instanceIdentifier}/record_collection_api`;
        console.log(`resource arn: ${dbUserArn}`);

        const dbConnectRole = new Role(this.scope, 'DatabaseConnectRole', {
            assumedBy: new ServicePrincipal('ecs-tasks.amazonaws.com')
        });

        dbConnectRole.attachInlinePolicy(
            new Policy(this.scope, 'DBAccessPolicy', {
                statements: [
                    new PolicyStatement({
                        actions: ['rds-db:connect'],
                        effect: Effect.ALLOW,
                        resources: [dbUserArn]
                    })
                ]
            })
        );

        // output the endpoint so we can connect!
        new CfnOutput(this.scope, 'RDS Endpoint', {value: rdsInstance.dbInstanceEndpointAddress});

        return rdsInstance;
    }

}