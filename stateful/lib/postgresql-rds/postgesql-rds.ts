import {Stack} from "@aws-cdk/core";
import {CdkEcsRdsStackProps} from "../cdk-ecs-rds-stack-props";
import {DatabaseCredentials} from "./database-credentials";
import {Bastion} from "./bastion";
import {PostgresqlRdsInstance} from "./postgresql-rds-instance";
import {Secret} from "@aws-cdk/aws-secretsmanager";
import {DatabaseInstance} from "@aws-cdk/aws-rds";

export class PostgresqlRds {
    public readonly databaseCredentialsSecret: Secret;
    public readonly postgresqlInstance: DatabaseInstance;

    constructor(scope: Stack, props: CdkEcsRdsStackProps) {
        // Build Database Credentials
        this.databaseCredentialsSecret = new DatabaseCredentials(scope, props).create();

        // Build Bastion
        const bastionSecurityGroup = new Bastion(scope, props).create();

        // Build Database
        this.postgresqlInstance = new PostgresqlRdsInstance(scope, props, this.databaseCredentialsSecret, [bastionSecurityGroup]).create();

    }
}