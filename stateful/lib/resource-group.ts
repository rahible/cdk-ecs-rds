import {Aspects, IAspect, IConstruct, Stack} from '@aws-cdk/core';
import {CfnGroup} from "@aws-cdk/aws-resourcegroups";
import {CdkEcsRdsStackProps} from "./cdk-ecs-rds-stack-props";
import {StackMemberCreator} from "./stack-member-creator";


// Fix for https://github.com/aws/aws-cdk/issues/9040
class ResourceGroupsTaggingFixAspect implements IAspect {
    public visit(node: IConstruct): void {
        if (node instanceof CfnGroup) {
            const tagValues = node.tags.tagValues();
            node.addDeletionOverride("Tags");
            node.addPropertyOverride("Tags", Object.keys(tagValues).map(key => {
                return {
                    Key: key,
                    Value: tagValues[key]
                };
            }));
        }
    }
}

export class ResourceGroup implements StackMemberCreator {
    constructor(private scope: Stack, private props: CdkEcsRdsStackProps) {
        // Bug in adding tags https://github.com/aws/aws-cdk/issues/9040
        Aspects.of(scope).add(new ResourceGroupsTaggingFixAspect());
    }

    create(): CfnGroup {
        // Set up Resource Group
        const resourceGroup = new CfnGroup(this.scope, `${this.props.tags?.environment}Resources`, {
            name: `${this.props.tags?.environment}-resources`,
            description: `The resources for the ${this.props.tags?.environment} environment`,
            tags: this.props.tags
        });
        return resourceGroup;
    }
}