# Stateful Resources

## Prerequisites

1. A VPC already exists for the account.
1. A public subnet already exists for the account.
2. At least 2 private subnets already exist in different AZs for RDS configuration.