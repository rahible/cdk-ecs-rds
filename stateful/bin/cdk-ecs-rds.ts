#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { CdkEcsRdsStack } from '../lib/cdk-ecs-rds-stack';

//TODO: how to build bastion key and put it somewhere in AWS (secrets manager?)
//TODO: figure out how to attach the subnet and route table without ids.
const DEVELOPMENT = {
  tags: {
    application: 'album-catalog',
    environment: 'development'
  },
  description: 'An Application to Track an Album Collection',
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
  bastionKeyName: 'CloudCmdKeyPair',
  publicSubnet: {
    publicSubnetId: 'subnet-0a5428b0215c25db5',
    publicRouteTableId: 'rtb-069a8baed8837e1f3',
    publicSubnetAz: 'us-east-1a'
  },
  vpcTargetTag: {
    'Name': 'aws-controltower-VPC'
  }
};

const app = new cdk.App();
new CdkEcsRdsStack(app, `CdkEcsRdsStack-${DEVELOPMENT?.tags?.environment}`, DEVELOPMENT);
