import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as CdkEcsRds from '../lib/cdk-ecs-rds-stack';
import {CdkEcsRdsStackProps} from "../lib/cdk-ecs-rds-stack-props";

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new CdkEcsRds.CdkEcsRdsStack(app, 'MyTestStack', {} as CdkEcsRdsStackProps);
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
