# NOTE: bash command is not needed here because cdk will prefix it when ran.
# writes output for user data execution to a viewable log file
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
# update yum
sudo yum update -y
# install java
sudo amazon-linux-extras install postgresql10
