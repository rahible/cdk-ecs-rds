# Objective
> The [Objective, Key Results and initiatives (OKRs)](https://www.perdoo.com/how-to-write-great-okrs/) are used in defining the goal of this project.

**_To learn how to build out an entire infrastructure environment in AWS using TypeScript and the AWS CDK, allowing for multiple environments such as Production and Development._**:

` This objective seems a bit big...should I break it apart to smaller ones?`

# Key Results
> The outcomes used to measure completion of the Objective and to scope the work.

- The entire environment will be built using AWS CDK in TypeScript, the console will not be used except to test and troubleshoot the deployed code. 
- The environment will be ECS Fargate cluster with docker containers for the back end services.
- The environment will have an RDS PostgreSQL database.
- The RDS PostgreSQL database will allow for fail over within the same region.
- The Database will backup to Snapshots.
- The Frontend will be an Angular SPA calling to the backend services.
- The reference data will deploy automatically with the backend service.
- Authentication will be through Google login. 


# Initiatives
> The items that need to be completed to support the Objective and measured by the Key Results

`Strike through as initiatives are complete - need to prioritize and order dependencies`
- ~~Add useful descriptions to all constructs that support them.~~
- ~~Build a Development ECS Fargate Environment.~~
- Build and deploy the backend application code to ECS Fargate environment.
- Build and deploy the front end to an Edge Location.
- ~~Build the RDS Backend.~~
- Add backing up the RDS Backend to the Stack.
- ~~Add support for failing over the RDS Backend.~~
- Use Flyway to deploy reference data for the backend application to PostgreSQL. [CI/CD database pipeline using Code Commit, Code Deploy and Flyway](https://aws.amazon.com/blogs/database/building-a-cross-account-continuous-delivery-pipeline-for-database-migrations/) 
- Diagram the infrastructure as it is being built out in order to plan, map and track the progress.
- Set up incognito to manage login from Google identities.
- Change the infrastructure code to support building a production environment.
- ~~Add the tagging to all resources in the environment.~~
- ~~Add a resource group for items created by the stack.~~ 
- Break apart the stack to multiple stacks and figure out how to share data between them. Separate stateful resources,
such as database, alb creation and ecs from stateless such as the application we are deploying. See [AWS-Core](https://docs.aws.amazon.com/cdk/api/latest/docs/core-readme.html) and [CDK Best Practices](https://aws.amazon.com/blogs/devops/best-practices-for-developing-cloud-applications-with-aws-cdk/)

## Left Off
08/10/2021: I've ran into the issue with building out a database pipeline that in the environment I'm in (AWS Workspaces), that I cannot install docker. I can only work with docker through an EC2 or ECS image. I've decided to put this on hold for now and work through an AWS example that uses Cloudformation, but instead use CDK. Then learn what I used from that exercise to formulate how to do something similar here.

## Future Refactorings
08/10/2021:
- Use the documentation on CDK App and Stacks to separate the RDS creation from the ECS Creation.
- Use the python example that I came across to build out a "stage" commandline variable to load different stacks for different stages based on that switch.
- Rename "environment" to "stage" throughout the code. AWS calls multiple stacks of the same environment "stages" (e.g. development, test, qa, production).
