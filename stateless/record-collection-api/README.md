# Record Collection API

A backend RESTful API supporting CRUD operations against a postgresql database.

## API urls

| URL | Verb | Description |
|  ---------------  | ---------------  |   ---------------  |
| /artists | GET | A list of all artists/groups in the collection. |
| /artists | POST | Insert an artist/group in the collection. |
| /artists/{artistId} | DELETE | Delete an artist/group in the collection, will cascade the deletion of associated albums. |
| /records | GET | A list of all records in a collection. |
| /records | POST | Insert a Record. |
| /records/{recordId} | DELETE | Delete a Record. |


