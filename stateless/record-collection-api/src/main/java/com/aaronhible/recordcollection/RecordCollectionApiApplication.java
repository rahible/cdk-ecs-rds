package com.aaronhible.recordcollection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecordCollectionApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordCollectionApiApplication.class, args);
    }

}
